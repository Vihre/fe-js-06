const mainDiv = document.querySelector('#root');
const url = 'https://api.ipify.org/?format=json'


async function findIP() {
   try {
      const ipResponse = await fetch(url);
      const { ip } = await ipResponse.json();

      const addressResponse = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`);
      const { continent, country, regionName, city, district } = await addressResponse.json();
      console.log(continent, country, regionName, city, district);

      mainDiv.insertAdjacentHTML('beforeend', `
      <div class='location'>
         <h2>Your location</h2>
         <div>Continent - ${continent}</div>
         <div>Country - ${country}</div>
         <div>Region - ${regionName}</div>
         <div>District - ${district}</div>
      </div>`)
   } catch (error) {
      console.log(error);
   }
}
